﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3
{
    internal class Republiek : Land
    {
        private string _president;

        public Republiek(string naam, string hoofdstad, string president) : base(naam, hoofdstad)
        {
            President = president;
        }
        public string President 
        { 
            get { return _president; } 
            set 
            {
                if (!value.Any(char.IsDigit))
                {
                    if (value != null && value != string.Empty)
                    {
                        _president = value;
                    }
                    else
                    {
                        throw new Exception("Het staatshoofd werd niet ingevuld!");
                    }
                }
                else
                {
                    throw new Exception("Het staatshoofd kan geen cijfer bevatten!");
                }
            } 
        }
        public override string ToString()
        {
            return base.ToString() + $" (President(e) : {President})";
        }
    }
}
