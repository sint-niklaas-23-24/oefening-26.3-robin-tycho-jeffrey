﻿using System.Linq;
using System.Xml.Serialization;
using System;

namespace Oefening_26._3
{
    internal class Land
    {
        //Attributen
        private string _hoofdstad;
        private string _naam;
        private string _staatshoofd;
        //Constructors
        public Land() { }
        public Land( string naam, string hoofdstad)
        {
            Hoofdstad = hoofdstad;
            Naam = naam;
        }
        //Properties
        public string Hoofdstad
        {
            get { return _hoofdstad; }
            set 
            { 
                if (!value.Any(char.IsDigit))
                {
                    if (value != null && value != string.Empty)
                    {
                        _hoofdstad = value;
                    }
                    else
                    {
                        throw new Exception("De hoofdstad werd niet ingevuld!");
                    }
                }
                else
                {
                    throw new Exception("De hoofdstad kan geen cijfer bevatten!");
                }
            }
        }
        public string Naam
        {
            get { return _naam; }
            set 
            {
                if (!value.Any(char.IsDigit))
                {
                    if (value != null && value != string.Empty)
                    {
                        _naam = value;
                    }
                    else
                    {
                        throw new Exception("Het land werd niet ingevuld!");
                    }
                }
                else
                {
                    throw new Exception("Het land kan geen cijfer bevatten!");
                }
            }
        }
        //Methoden
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Land r = (Land)obj;
                    if (this.Naam == r.Naam && this.Hoofdstad == r.Hoofdstad)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return GetHashCode().ToString() + " | Land: " + Naam + ", Hoofdstad: " + Hoofdstad;
        }
    }
}
