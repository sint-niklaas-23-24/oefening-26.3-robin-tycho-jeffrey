﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_26._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Land> landen = new List<Land>();

        private void btnAddLand_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rdoMonarchie.IsChecked == true)
                {
                    Monarchie monarchie = new Monarchie(txtCode.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                    if (landen.Contains(monarchie) == false) 
                    {
                        landen.Add(monarchie);
                    }
                    else
                    {
                        throw new Exception("Dit land bestaat al!");
                    }
                }
                else if (rdoRepubliek.IsChecked == true)
                {
                    Republiek republiek = new Republiek(txtCode.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                    if (landen.Contains(republiek) == false)
                    {
                        landen.Add(republiek);
                    }
                    else
                    {
                        throw new Exception("Dit land bestaat al!");
                    }
                }
                else if (rdoOverig.IsChecked == true)
                {
                    Land land = new Land(txtCode.Text, txtHoofdstad.Text);
                    if (landen.Contains(land) == false)
                    {
                        landen.Add(land);
                    }
                    else
                    {
                        throw new Exception("Dit land bestaat al!");
                    }
                }
                else
                {
                    throw new Exception("Er werd geen type aangevinkt!");
                }
                RefreshListbox();
                ClearTextBoxes();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void rdoMonarchie_Checked(object sender, RoutedEventArgs e)
        {
            txtStaatshoofd.Visibility = Visibility.Visible;
            lblStaatshoofd.Visibility = Visibility.Visible;
        }
        private void rdoRepubliek_Checked(object sender, RoutedEventArgs e)
        {
            txtStaatshoofd.Visibility = Visibility.Visible;
            lblStaatshoofd.Visibility = Visibility.Visible;
        }
        private void rdoOverig_Checked(object sender, RoutedEventArgs e)
        {
            txtStaatshoofd.Visibility = Visibility.Hidden;
            lblStaatshoofd.Visibility = Visibility.Hidden;
        }
        private void RefreshListbox()
        {
            lbInhoud.ItemsSource = null;
            lbInhoud.ItemsSource = landen;
        }
        private void ClearTextBoxes()
        {
            txtCode.Text = string.Empty;
            txtHoofdstad.Text = string.Empty;
            txtStaatshoofd.Text = string.Empty;
        }
    }
}
